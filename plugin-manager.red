Red [
    Title: "Plugin manager"
    Needs: 'View
]

pm: context [
    plugins: #()    
    aggregators: #()

    data: []

    ;propagate drop-list
    progagate_draw-types: does [
        mold append/only draw-types data
    ]

    load: does [
        ;draw-type plugins        
        foreach file read %draw-types/ [
            print ["file:" file "| name:" name: copy/part file (length? file) - 4]
            append data to string! name
            put plugins
                to string! name
                do read rejoin [%draw-types/ file]
                
                ;probe file
                comment {
                put plugins
                    to string! name
                    do read file
                    ;probe plugins
                }
                ;append plugins reduce [to string! do read file]
                ;probe plugins
            ;]            
        ]
    ]

    comment {
    ;aggregator plugins
    foreach file read %. [
        if find file "aggregator" [
            print ["file:" file "| name:" name: copy/part file (length? file) - 4]
            append data to string! name
            put types
                to string! name
                do read file
                ;probe types
        ]
    ]
    }

    ;print ["propagated:" progagate_draw-types]

    ;print ["types:" plugins]
]

comment {
window: [
    drop-list "test" data pm/data on-change [
            print pick face/data face/selected
        ]
]

view window
}