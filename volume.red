Red [
    Title: "Volume"
    Type:  "Indicator"
    Needs: 'View
]

volume: context [
    plot: func [
        data                [block!]

        /local
            rule            [block!]
            max-volume      [float!]
            min-volume      [float!]
            block           [block!]
            normalized      [float!]
            scale           [float!]
            entry           [block!]
            count           [integer!]
            idx             [integer!]
            left-bottom     [integer!]
            right-top       [integer!]
            entries         [integer!]
            value           [integer!]            
            range           [float!]
            values          [block!]
            position        [pair!]
            period          [integer!]
            maximum         [integer!]
            step            [integer!]
            width           [integer!]
            border-color    [lit-word! word! tuple!]
            body-color      [lit-word! word! tuple!]
            plugin          [object!]

        return:             [block!]
    ][              
        rule: [
            set values          block!
            set position        pair!
            set period          integer!
            set maximum         integer!
            set step            integer!
            set width           integer!
            set border-color    [lit-word! | word! | tuple!]
            set body-color      [lit-word! | word! | tuple!]
            set plugin          [object!]
        ]
 
        block: copy []

        print ["VOLUME--------------"]
        probe data

        if parse data rule [        
            
            left-bottom: position
            right-top: position
            min-volume: 1000000.0
            max-volume: 0.0
            length: length? values
            period: either period > length [length][period]

            repeat count period [
                idx: period - (count - 1)                            

                if values/:idx > max-volume [max-volume: values/:idx]
                if values/:idx < min-volume [min-volume: values/:idx]
            ]

            print ["min-volume:" min-volume "max-volume:" max-volume]

            range: max-volume - min-volume

            block: []

            probe plugin

            start: now/time/precise
            repeat count period [            
                idx: period - (count - 1)     

                value: to-integer ((to-float values/:idx) - min-volume) / range * maximum
                append block plugin/plot reduce [value position width border-color body-color]
                position/x: position/x + step
            ]         
            end: now/time/precise
            print ["start:" start "end:" end "timing loop-volume:" end - start]
        ]
        
        block                
    ]
]

;view [base 1000x800 draw volume/plot [[10 20 30 40] 100x400 5 100 15 10 yellow black box]]
;view [base 1000x800 draw volume/plot [[10 20 30 40] 100x400 5 50 15 10 white blue box]]
;view [base 1000x800 draw volume/plot [[10 20 30 40] 100x400 2 230 15 10 white blue box]]