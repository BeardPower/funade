Red [
    Title: "Square draw type"
    Type: "Drawtype"
    Needs: 'View
]

square: context [
    plot: func [
        data                [block!]

        /local
            rule            [block!]
            max-value       [float!]
            min-value       [float!]
            block           [block!]
            normalized      [float!]
            scale           [float!]
            entry           [block!]
            count           [integer!]
            idx             [integer!]
            left-bottom     [integer!]
            right-top       [integer!]
            entries         [integer!]
            value           [integer!]            
            range           [float!]
            values          [block!]
            position        [pair!]
            period          [integer!]
            maximum         [integer!]
            step            [integer!]
            width           [integer!]
            border-color    [lit-word! word! tuple!]
            body-color      [lit-word! word! tuple!]
            plugin          [object!]

        return:             [block!]
    ][              
        rule: [
            set values          block!
            set position        pair!
            set period          integer!
            set maximum         integer!
            set step            integer!
            set width           integer!
            set border-color    [lit-word! | word! | tuple!]
            set body-color      [lit-word! | word! | tuple!]
        ]
 
        block: copy []

        if parse data rule [        
            
            left-bottom: position
            right-top: position
            min-value: 1000000.0
            max-value: 0.0
            length: length? values
            period: either period > length [length][period]

            repeat count period [
                idx: period - (count - 1)                            

                if values/:idx > max-value [max-value: values/:idx]
                if values/:idx < min-value [min-value: values/:idx]
            ]

            ;print ["min-value:" min-value "max-value:" max-value]

            range: max-value - min-value

            block: compose ['fill-pen (body-color) 'pen (border-color)]

            start: now/time/precise
            repeat count period [            
                idx: period - (count - 1)     
                
                value: to-integer ((to-float values/:idx) - min-value) / range * maximum
                append block compose ['box as-pair (position/x - (width * 0.5)) (position/y - value - (width * 0.5)) as-pair (position/x + (width * 0.5)) (position/y - value + (width * 0.5))]
                position/x: position/x + step
            ]         
            end: now/time/precise

            ;print ["start:" start "end:" end "timing loop-volume:" end - start]
        ]
        
        reduce block                
    ]
]

;comment {
;data: []
;loop 5000 [append data random 100]
;result: square/plot reduce [data 100x400 5000 100 15 10 yellow black]

;view [base 1000x800 draw result]
;}
