Red [
    Title: "Bar draw type"
    Needs: 'View
]

;aggreate body [box width height color border-width border-color] wick [line ]
;body [box open close 10 black 1 yellow] wick [line high 10 black] wick [line low 10 yellow]
;aggregate 10min 

;plugin:
bar: context [
    rule: [
        set body 'body
        set body-color tuple!
        opt ['wick set wick-color tuple!]
    ]

    generate: function[
        ;timestamp       [date!]
        timestamp       [pair!]
        open            [float!]
        high            [float!]
        low             [float!]
        close           [float!]
        volume          [float!]
        width           [integer!]
        body-color-up   [tuple!]
        body-color-down [tuple!]
        /border
        border-color    [tuple!]
        /wicks
        wick-color      [tuple!]

        /local
            block           [block!]
            start           [pair!]
            end             [pair!]
            marker-open     [pair!]
            marker-close    [pair!]
            offset          [integer!]
            up?             [logic!]
            body-color      [tuple!]
 
        return:             [block!]
    ][

        up?: close >= open
        start: timestamp
        end: start
        marker-open: start
        marker-close: start
        start/y: 400 - to integer! low
        end/y: start/y - to integer! absolute (high - low)
        marker-open/y: 400 - to integer! open
        marker-close/y: 400 - to integer! close
        ;print ["timestamp:" timestamp "offset:" to integer! (open - close)] 
        ;print ["start:" start]
        ;print ["end:" end]
        ;print ["marker-open:" marker-open "marker-close:" marker-close]
        
        either up? [
                body-color: body-color-up
            ]
            [
                body-color: body-color-down
            ]

        ;print ["body-color-up" body-color-up "body-color-down:" body-color-down "body-color:" body-color]

        block: copy ['pen body-color 'line-width 1 'fill-pen 'off 'line start end]
        ;print [start end]

        if not wicks [wick-color: body-color]
        
        append block quote ['pen wick-color
                            ;'line-cap 'round
                            'line-width 1
                            'line marker-open as-pair (marker-open/x - (width * 0.5)) marker-open/y
                            'line marker-close as-pair (marker-close/x + (width * 0.5)) marker-close/y
        ]
   
        ;print ["block:" block lf "reduced block:" reduce block]
        reduce block
    ]

    draw-type: function [input [block!]] [
        print ["input:" input]
        probe parse input rule
        print ["body-color:" body-color]
        print ["wick-color:" wick-color]
    ]

    comment {
plugin/draw-type [body 255.255.255.0 wick 135.255.255.0]
plugin/draw-type [body 255.255.255.0]

plugin/generate now 10.0 20.0 0.0 15.0 123.3 10 black
plugin/generate/border now 10.0 20.0 0.0 15.0 123.3 10 black red
plugin/generate/wicks now 10.0 20.0 0.0 15.0 123.3 10 black blue
plugin/generate/border/wicks now 10.0 20.0 0.0 15.0 123.3 10 black red blue
plugin/generate/wicks/border now 10.0 20.0 0.0 15.0 123.3 10 black blue red
}
]

;draw-type/generate 20x200 200.0 300.0 180.0 270.0 123.3 10 green red
;draw-type/generate 20x200 200.0 300.0 100.0 150.0 123.3 10 green red