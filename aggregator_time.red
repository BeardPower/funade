Red [
    Title: "Time Aggregator"
    Needs: 'View
]

data: []

datapoint: context [
    timestamp:  now
    open:       0.0
    high:       0.0
    low:        0.0
    close:      0.0
    volume:     0.0
]

debug-print: function [
    datapoint
]
[
    print ["timestamp:" tab datapoint/timestamp lf "open:" tab tab datapoint/open lf "high:" tab tab datapoint/high lf "low:" tab tab datapoint/low lf "close:" tab tab datapoint/close lf "volume:" tab datapoint/volume]
]

plugin: context [
    aggregate: function [
        period [word!]
        value  [integer!]
        data   [block!]
        /local
            minutes [integer!]
            index   [integer!]
            tmp     [integer!]
    ]
    [
        print ["period:" period "value:" value]

        minutes: either period = 'hour [60 * value][value]

        print ["minutes:" minutes]

        index: 2
        tmp: 1

        foreach datapoint data [    
            ;debug-print datapoint
            ;print datapoint/timestamp/minute
            tmp: index - 1
            
            print ["diff:" (data/:index/timestamp - data/:tmp/timestamp)]

            if index > 1 [print ["diff:" datapoint/timestamp - datapoint/timestamp]]
            ; if (remainder datapoint/timestamp/minute minutes) = 0 [print ["minute:" datapoint/timestamp/minute]]

            index: index + 1
        ]
    ]
]

data: []

time: now/precise
index: 1

loop 120 [
    period: time
    period/minute: 1 * index
    append data make datapoint [timestamp: period]
    ;debug-print data/:index
    
    comment {
    if index > 1 [
        print data/:index/timestamp
        print data/(:index - 1)/timestamp
        print ["minutes diff:" diff: data/:index/timestamp/time - data/(:index - 1)/timestamp/time "diff:" diff/minute] 
        ]
    }
    index: index + 1
]

plugin/aggregate 'minute 20 data
plugin/aggregate 'hour 1 data
plugin/aggregate 'hour 2 data