Red [
    Title: "Box draw type"
    Type: "Drawtype"
    Needs: 'View
]

box: context [
    plot: func [
        data                [block!]

        /local
            rule            [block!]
            block           [block!]
            value           [integer! float!]
            position        [pair!]
            width           [integer!]
            border-color    [lit-word! word! tuple!]
            body-color      [lit-word! word! tuple!]

        return:             [block!]
    ][
        rule: [
            set value           [integer! | float!]
            set position        pair!
            set width           integer!
            set border-color    [lit-word! | word! | tuple!]
            set body-color      [lit-word! | word! | tuple!]
        ]

        block: copy []       

        if parse data rule [
            block: compose ['fill-pen (body-color) 'pen (border-color) 'box position as-pair position/x + width position/y - value]
        ]

        reduce block
    ]
]

;comment {
    ;view [base 400x400 draw box/plot [10 200x200 20 green black]]
    ;view [base 400x400 draw box/plot [25 200x250 10 yellow red]]
;}


