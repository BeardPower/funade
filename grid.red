Red [
    Title: "Grid"
    Type:  "Background"
    Needs: 'View
]

grid: context [

    generate: function [       
        dimension               [pair!]
        main-resolution         [pair!]
        sub-resolution          [pair!]
        color-main              [word! tuple!]
        width-main              [integer!]
        color-sub               [word! tuple!]
        width-sub               [integer!]

        /local
            block               [block!]
            amount-main-lines   [pair!]
            amount-sub-lines    [pair!]
            count               [integer!]
            start               [integer!]
            stop                [integer!]

        return:                 [block!]
    ][    
        ;print ["dimension:" dimension "main-resolution:" main-resolution "sub-resolution:" sub-resolution]

        amount-main-lines: dimension / main-resolution
        amount-sub-lines: dimension / sub-resolution        
        
        ;print ["amount-main-lines:" amount-main-lines "amount-sub-lines:" amount-sub-lines]
        
        block: copy ['pen color-sub]

        count: 0
        repeat count amount-sub-lines/x + 1 [
            start: sub-resolution/x * (count - 1)
            ;print ["start:" start]
            append block compose ['line-width (width-sub) 'line as-pair (start) 0 as-pair (start) (dimension/y)]
        ]

        count: 0
        repeat count amount-sub-lines/y + 1 [
            start: sub-resolution/y * (count - 1)
            ;print ["start:" start]
            append block compose ['line as-pair 0 (start) as-pair (dimension/x) (start)]
        ]
   
        append block ['pen color-main]

        count: 0
        repeat count amount-main-lines/x + 1 [
            start: main-resolution/x * (count - 1)
            ;print ["start:" start]
            append block compose ['line-width (width-main) 'line as-pair (start) 0 as-pair (start) (dimension/y)]
        ]

        count: 0
        repeat count amount-main-lines/y + 1 [
            start: main-resolution/y * (count - 1)
            ;print ["start:" start]
            append block compose ['line as-pair 0 (start) as-pair (dimension/x) (start)]
        ]

        ;print ["block:" block lf "reduced block:" reduce block]
        
        reduce block        
    ]
]

;view [base 1000x500 draw grid/generate 1000x500 50x50 10x10 black 2 blue 1]