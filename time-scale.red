Red [
    Title: "Time-Scale"
    Type:  "Scale"
    Needs: 'View
]

time-scale: context [

    generate: function [      
        face        [object!] 
        space       [integer!]
        data-width  [integer!]

        /local
            block             [block!]
            amount-of-items   [integer!]
            text-size         [pair!]

        return:                 [block!]
    ][
        block: []

        text-size: size-text/with face "00-00-0000"
      
        amount-of-items: space / text-size/x
        
        print ["amount-of-items:" amount-of-items]

        reduce block        
    ]
]

main: [base 1000x400]
view/no-wait layout main

time-scale/generate system/view/screens/1/pane/1/pane/1 400 10

print type? system/view/screens/1/pane/1/pane/1