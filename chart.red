Red [
    Title: "Chart base"
    Needs: 'View
]

;#include %crosshair.red

chart: context [
    sub-data: copy []
    selected-plugin: []
    draw-block: []
    last-id: 1
    delimiter: #"_"
    item: [
            timestamp:  2019-01-01
            open:       0.0
            high:       0.0
            low:        0.0
            close:      0.0
            volume:     0.0
        ]

    symbol: [
        name: "dummy"
        ticksize: 0.0
        data: []
    ]

    face: make face! [
        type: 'base
        size: 100x100
        color: red
        actors: object [
            on-create: func [face [object!]][
                print "created"
            ]

            on-down: func [face [object!] event [event!]][
                print "down"
                ;crosshair-block/handle-events event
            ]
        ]
        extra: [
            scale-factor: 0.0
        ]
    ]

    plugins: make face! [
        #include %plugin-manager.red
        plugin-manager: pm
        plugin-manager/load

        print ["plugin-manager/plugins:" plugin-manager/plugins]
        print ["plugin-manager/data:" plugin-manager/data]

        type: 'drop-list
        size: 100x100
        text: "plugins"
        data: plugin-manager/data

        actors: object [
            on-change: func [face [object!] event [event!]][
                print pick plugins/data plugins/selected
                code: pick plugins/data plugins/selected
                selected-plugin: plugin-manager/plugins/(code)
                ;probe selected-plugin
                ;probe draw-block/volume_1

                clear draw-block/volume_1
                draw-block/volume_1: selected-plugin/plot reduce [sub-data 12x200 5000 100 10 10 'black 'yellow]
            ]
        ]
    ]   

    init: func [
        size    [pair!]
        color   [tuple!]

        return: [object!]
    ]
    [
        face/size: size
        face/color: color
        face/pane: reduce [plugins]

        selected-plugin: plugins/plugin-manager/plugins/("box")

        ;probe selected-plugin

        face       
    ]

    draw: func [        
    ]
    [
        face/draw: draw-block
    ]

    add-component: func [
        data [block!]

        /local
            name        [word!]
            ;sub-data    [block!]
    ]
    [   

        name: to-word rejoin [data/1 delimiter last-id]
        last-id: last-id + 1
        append draw-block to set-word! name
        append/only draw-block copy []
        
        switch data/1 [
            'crosshair [
                print "crosshair"
                ;#include %crosshair.red
                draw-block/(name): crosshair/generate draw-block data/2 data/3
            ]
            'grid [
                print "grid"
                ;#include %grid.red
                draw-block/(name): grid/generate data/2 data/3 data/4 data/5 data/6 data/7 data/8
            ]
            'candlestick [
                print "candlestick"
                ;#include %candle-stick.red                

                ;append/only draw-block []

                step: 20
                count: 0
                idx: 0
                length: length? symbol/data

                repeat count length [
                    idx: length - count + 1                
                    
                    append draw-block/(name) candlestick/generate/border/wicks as-pair (step * count - 1) 0 symbol/data/:idx/open / symbol/ticksize symbol/data/:idx/high / symbol/ticksize symbol/data/:idx/low / symbol/ticksize symbol/data/:idx/close / symbol/ticksize symbol/data/:idx/volume 10 green red black blue                                        

                    ;print ["candle #:" count "open:" symbol/data/:idx/open "close:" symbol/data/:idx/close "ticksize:" symbol/ticksize "size:" symbol/data/:idx/open - symbol/data/:idx/close / symbol/ticksize]
                ]
            ]
            'scale [
                print "scale"
                ;#include %scale.red
                draw-block/(name): scale/generate data/2 data/3 data/4 data/5 data/6 data/7 data/8 data/9 data/10 get data/11                
            ]
            'volume [
                print "volume"
                #include %volume.red
                
                ;repeat idx 200 [
                ;    append sub-data symbol/data/:idx/volume
                ;]

                sub-data: compose [
                    (16-03-2019 - 03-03-2019) (03-03-2019 - 16-02-2019) (16-02-2019 - 05-02-2019)
                    (05-02-2019 - 12-01-2019) (12-01-2019 - 29-12-2018) (29-12-2018 - 20-12-2018)
                    (20-12-2018 - 09-12-2018) (09-12-2018 - 15-11-2018) (15-11-2018 - 20-10-2018)
                    (20-10-2018 - 06-10-2018) (06-10-2018 - 26-09-2018) (26-09-2018 - 16-09-2018)
                    (16-09-2018 - 03-09-2018) (03-09-2018 - 22-08-2018) (22-08-2018 - 13-08-2018)
                    (13-08-2018 - 01-08-2018) (01-08-2018 - 08-07-2018) (08-07-2018 - 03-06-2018)
                    (03-06-2018 - 08-05-2018) (08-05-2018 - 23-04-2018) (23-04-2018 - 31-03-2018)
                    (31-03-2018 - 18-03-2018) (18-03-2018 - 10-03-2018) (10-03-2018 - 14-02-2018)
                ]

                probe sub-data
                ; draw-block/(name): volume/plot reduce [sub-data 0x200 100 180 25 25 'black 'yellow 'box]
                
                name: to-word rejoin [data/1 delimiter last-id]
                last-id: last-id + 1
                append draw-block to set-word! name
                append/only draw-block copy []

                ;draw-block/(name): volume/plot reduce [sub-data 12x200 100 180 25 10 'blue 'white 'dot]

                ;comment {
                ;coin-data: read https://api.coingecko.com/api/v3/coins/red/market_chart?vs_currency=eur&days=7
                ;coin-data: {{"data":"test"}}

                ;#include %json.red
                ;decoded: json/decode coin-data

                ;print "decoded............."
                ;probe decoded
                sub-data: []

                comment {
                foreach data decoded/prices [
                    append sub-data data/2
                ]
                }
                
                print "hello ------------------------->"
                loop 5000 [append sub-data random 100]

                probe selected-plugin
               
                draw-block/volume_1: selected-plugin/plot reduce [sub-data 12x200 5000 100 10 10 'black 'yellow]                 
                ;}
                ;draw-block/(name): volume/plot reduce [sub-data 12x200 1000 100 10 10 'black 'yellow selected-plugin]
     
                ;draw-block/(name): volume/plot sub-data data/2 data/3 data/4 data/5 data/6 data/7 data/8 'doty
            ]
            'sma [
                ;#include %sma.red
                
                sub-data: copy []
                
                repeat idx 200 [
                    append sub-data symbol/data/:idx/close
                ]

                ;probe sub-data
                probe data
                draw-block/(name): sma/generate sub-data data/2 data/3 data/4 data/5 data/6 data/7 data/8 data/9 data/9 'doty
            ]
        ]
        
        
        ;append draw-block reduce [cross-hair: reduce t]
        ;append draw-block reduce [cross-hair: t]

        ;probe draw-block/cross-hair

        ;append/only draw-block [cross-hair: crosshair/generate draw-block 50x50 100x100]
        ;append draw-block crosshair/generate draw-block 200x200 250x250

        ;probe draw-block
        ;print ["setting chart type to:" type]
    ]

    load-data: func [
        file    [file!]

        /local
            lines   [block!]
            data    [block!]
            count   [integer!]
            amount  [integer!]
    ]
    [
        lines: read/lines file
        amount: length? lines
        symbol/data: reduce append/dup [] [copy item] amount
        data: []
        count: 1

        symbol/name: "ETHUSD"
        symbol/ticksize: 0.01

        foreach element lines [
            data: reduce split element #","            
            symbol/data/:count/timestamp:   load data/1
            symbol/data/:count/open:        to-float data/3
            symbol/data/:count/high:        to-float data/4
            symbol/data/:count/low:         to-float data/5
            symbol/data/:count/close:       to-float data/6
            symbol/data/:count/volume:      to-float data/7            

            count: count + 1
        ]        
    ]
]

comment {
    draw: []
    crosshair-block: []

    base: make face! [
        type: 'base
        color: gray
        actors: object [
            on-down: func [face [object!] event [event!]][
                ;print "down"
                crosshair-block/handle-events event
            ]
        ]        
    ]

    init: func [
        size        [pair!]

        /local
            block   [block!]

        return:     [block!]
    ]
    [
        base/size: size
        ;base/actors: none

        crosshair-block: crosshair/generate draw size * 0.5 size
        ;draw: crosshair/generate draw size * 0.5 size

        probe draw
        base/draw: crosshair-block

        reduce [base]
    ]
]
}

window: make face! [
    type: 'window text: "Funade" size: 700x300
]


;window/pane: chart/init 400x400
;probe window

mychart: chart
mychart/init 1000x300 white
;mychart/load-data %Kraken_ETHUSD_d.csv
;mychart/add-component ['grid 610x200 610x200 610x200 Black 2 Gray 1]
;mychart/add-component ['grid 500x500 100x100 25x25 Black 2 Gray 1]
;mychart/add-component ['grid 500x500 500x500 500x500 Black 1 Gray 1]
;mychart/add-component ['crosshair 50x50 100x100]
;mychart/add-component ['crosshair 150x150 200x200]
;mychart/add-component ['crosshair 25x250 300x300]
;mychart/add-component ['candlestick 30x30 100.0 150.0 20.0 140.0 123.45 10 Green Red black blue]
;mychart/add-component ['candlestick 100x100 300.0 310.0 20.0 200.0 123.45 10 Green Red black blue]
;mychart/add-component compose ['scale 0x500 500x200 (now/date) 1 100 1 10 blue 10 no]
;mychart/add-component compose ['scale 12x200 575x300 1 1 25 1 10 red 10 no]
mychart/add-component ['volume 50 20x200 200 25 8 black yellow]
;mychart/add-component compose ['scale 610x200 200x200 1 2 12 1 10 blue 10 yes]
;mychart/add-component ['volume 50 10x600 30 20 8 red cyan]
;mychart/add-component ['volume 50 10x650 30 20 8 red off]
;mychart/add-component ['volume 50 10x700 30 20 8 off blue]
;mychart/add-component ['sma 1 30 100x200 20 20 8 black blue]
;mychart/add-component ['sma 2 30 100x200 20 20 8 black red]
;mychart/add-component ['sma 14 30 100x200 20 20 8 black green]
;mychart/add-component ['sma 200 30 100x200 20 20 8 black yellow]
;probe mychart/draw-block

mychart/draw

window/pane: reduce [mychart/face]

system/view/debug?: no
system/view/auto-sync?: yes
view window