Red [
    Title: "Crosshair"
    Type:  "Tool"
    Needs: 'View
]

crosshair: context [

    block: []

    generate: function [
        block           [block!]
        cross           [pair!]
        dimension       [pair!]

        /local
            start           [pair!]
            end             [pair!]
            marker-open     [pair!]
            marker-close    [pair!]
            offset          [integer!]
            up?             [logic!]
            body-color      [tuple!]

        return:             [block!]
    ][
        
        ;print ["cross/y:" cross/y "dimension/y:" dimension/y]
        if cross/y > dimension/y [cross/y: dimension/y]

        block: ['pen 'red 'line as-pair 0 cross/y as-pair dimension/1 cross/y 'line as-pair cross/x 0 as-pair cross/x dimension/1 'text cross - -10x20 form reduce [cross]]
   
        ;print ["block:" block lf "reduced block:" reduce block]
        reduce block
    ]

    handle-events: func [
        event   [event!]
    ]
    [
        ;probe event
        print ["event:" event]
    ]
]