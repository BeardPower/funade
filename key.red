Red [
    Title: "Key"
    Type: "Legend"
    Needs: 'View
]

key: context [
    plot: func [
        data        [block!]

        /local
            rule    [block!]
            block   [block!]

        return:         [block!]
    ][

        rule: [
            set value           [integer! | float!]
            set label           [string!]
            set position        [pair!]
            set width           [integer!]
            set border-color    [lit-word! | word! | tuple!]
            set body-color      [lit-word! | word! | tuple!]
        ]

        block: copy []

        if parse data rule [
            block: compose ['fill-pen (body-color) 'pen (border-color) 'box position position + width 'text as-pair position/x + (width * 2) position/y - (width / 2) append label ":" 'text as-pair position/x + (width * 10) position/y - (width / 2) to-string value]
        ]

        reduce block
    ]
]

;comment {
    view [base 400x400 draw key/plot [10 "SMA(20)" 50x50 10 black green]]
    view [base 400x400 draw key/plot [10 "RIS(2)" 250x50 5 black yellow]]
;}
