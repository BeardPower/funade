Red [
    Title: "SMA"
    Type:  "Indicator"
    Needs: 'View
]

sma: context [
    generate: func [
        data            [block!]
        period          [integer!]
        length  	    [integer!]
        position        [pair!]
        height          [integer!]
        step            [integer!]
        width           [integer!]
        color-border    [lit-word! word! tuple!]
        color-body      [lit-word! word! tuple!]
        drawtype        [word!]

        /local
            max-value   [float!]
            min-value   [float!]
            value       [float!]
            block       [block!]
            normalized  [float!]
            entry       [block!]
            count       [integer!]
            inner-count [integer!]
            inner-idx   [integer!]
            idx         [integer!]
            left-bottom [integer!]
            right-top   [integer!]
            entries     [integer!]

        return:         [block!]
    ][  
        left-bottom: position
        right-top: position
        min-value: 1000000.0        
        max-value: 0.0
    
        entries: length? data
        length: either length > entries [entries][length]
        period: either period > length [length][period]
        
        repeat idx length [
            if data/:idx > max-value [max-value: data/:idx]
            if data/:idx < min-value [min-value: data/:idx]
        ]        

        print ["length:" length "entries:" entries]
        normalized: max-value / (to-float height)

        print ["min-value:" min-value "max-value:" max-value "normalized:" normalized]

        block: ['pen color-border 'fill-pen color-body]
        
        left-bottom: position

        count: 0
        idx: 0
        
        repeat count length [
            idx: entries - (count - 1)     
            left-bottom/x: position/x + (step * (count - 1))
            ;print ["height:" height "volume:" data/:idx/volume "normalized:" normalized "/:" to-integer data/:idx/volume / normalized]

            print ["idx:" idx "data:" data/:idx]
            ; inner calculation loop
            value: 0
            repeat inner-count period [
                inner-idx: idx - inner-count + 1
                print ["inner-idx:" inner-idx "data:" data/:inner-idx "value:" value]
                value: value + data/:inner-idx                
                print value
            ]

            value: value / period

            print ["sma(" period "):" value "normalized:" to-integer value / normalized] 
            
            ;append block compose ['line as-pair left-bottom/x left-bottom (right-top)]

            print left-bottom            

            left-bottom/x: position/x + (step * (count - 1))

            print left-bottom/x

            ;left-bottom/y: left-bottom/y - to-integer data/:idx / normalized

            append block compose ['circle as-pair (left-bottom/x) (left-bottom/y + value) 5]
            ;right-top/x: left-bottom/x + width
            ;right-top/y: left-bottom/y - to-integer data/:idx / normalized
            ;probe compose ['box (left-bottom) (right-top)]

            comment {
            either drawtype = 'dot [
                #include %dot.red  
                append block reduce [dot/generate (left-bottom) ((to-integer data/:idx / normalized) / 5) color-body color-border]
            ]
            [
                append block reduce ['line left-bottom right-top]
                ;append block compose ['box (left-bottom) (right-top)]
            ]        
            }    
        ]        
        
        probe reduce block
        reduce block                
    ]
]

;view [base 1000x500 draw grid/generate 1000x500 50x50 10x10 black 2 blue 1]