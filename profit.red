Red [
    Title: "Profit calculator"
    Needs: 'View
]

rule: [
    [integer! | float!]
    [integer! | float!]
    [integer!]
    [integer! | float!]
    [integer!]
    [integer!]
]

validate: func [
    block   [block!]

    return: [logic!]  
]
[
    parse block rule
]

calculate: func [
    equity  [integer! float!]
    risk    [integer! float!]
    losing  [integer!]
    factor  [integer! float!]
    trades  [integer!]
    runs    [integer!]
        
    /auto

    /local
        profit      [float!]
        loss?       [logic!]
        value       [integer!]
        winners     [integer!]
        losers      [integer!]
        blown       [string!]
        balance     [integer! float!]
        probability [integer!]
        profitable-winners [integer!]
        profitable-losers  [integer!]

    return: [block!]
]
[
    total: 0
    balance: equity
    winners: 0
    losers: 0
    profit: equity * (risk / 100.0)
    sum-winners: 0
    sum-losers: 0
    probability: 0

    either not auto [
        random/seed now
        loop trades [if balance < equity [break] value: random 100 either value <= losing [losers: losers + 1 balance: balance - profit][winners: winners + 1 balance: balance + (profit * factor)]]
    ][
        loop runs [
            probability: 0            

            while [all [balance >= equity probability <= 100]][
                profitable-winners: winners
                profitable-losers: losers
                balance: equity
                probability: probability + 1            
                winners: 0
                losers: 0

                random/seed now            
                loop trades [value: random 100 either value <= probability [losers: losers + 1 balance: balance - profit][winners: winners + 1 balance: balance + (profit * factor)]]                                          
            ]
            
            sum-winners: sum-winners + winners
            sum-losers: sum-losers + losers
        ]

        winners: sum-winners / runs
        losers: sum-losers / runs
    ]
    blown: either balance >= equity ["no"]["yes"]
    losing: to integer! (losers / to float! (winners + losers)) * 100

    compose [quote blown: (blown) quote balance: (balance) quote winners: (winners) quote losers: (losers) quote losing: (losing)]
]

view [
    title "Profit calculator"
    style label: text right 100x24
    across
    label "Equity:" equity: field "10000" return
    label "Risk (%):" risk: field "10" return
    label "Losing trades (%):" losing: field "10" return
    label "Reward factor:" factor: field "1" return
    label "Trades:" trades: field "1000" return    
    label "Runs:" runs: field "100" return    
    label "Winners:" winners: text "0" return    
    label "Losers:" losers: text "0" return    
    label "Balance:" balance: text "0" return
    label "Account blown:" blown: text "no" return
    label "Status:" status: text "Ready" return

    button "Auto" [        
        either not validate reduce [equity/data risk/data losing/data factor/data trades/data runs/data][
            status/data: "Invalid input!"
        ]
        [               
            status/data: "Calculating..."
            losing/data: 0
            results: calculate/auto equity/data risk/data losing/data factor/data trades/data runs/data
            losing/data: results/losing
            winners/data: results/winners
            losers/data: results/losers
            balance/data: results/balance
            blown/data: results/blown
            status/data: "Ready"
        ]
    ]
    button "Calculate" [        
        either not validate reduce [equity/data risk/data losing/data factor/data trades/data runs/data][
            status/data: "Invalid input!"
        ]
        [            
            status/data: "Calculating..."            
            results: calculate equity/data risk/data losing/data factor/data trades/data runs/data         
            winners/data: results/winners
            losers/data: results/losers
            balance/data: results/balance
            blown/data: results/blown
            status/data: "Ready"
        ]
    ]
]

   comment {
    below left
    text "Equity"
    text "Losing trades (%)"
    text "Trades"
    return
    below right
    equity: field options [default: 10000]
    losing: field options [default: 25%]
    trades: field options [default: 100]
    below center
    calc: button "Calc"
    result: text
}