Red [
    Title: "Test"
    Needs: 'View
]

#include %candle-stick.red
#include %bar.red
#include %p_f.red
#include %crosshair.red
#include %grid.red
#include %css-colors.red

start:      1.0
open:       1.0
high:       1.0
low:        1.0
close:      1.0
volume:     1000.0
body-epsilon:    30%
wick-epsilon:    5%
scale: 200
idx: 1
open-old: 1.0
close-old: 1.0
s: 1.0
left-down?: no
alt-down?: no
mid-down?: no
old-offset: 0x0
draw-block: []
tmp: []
grid-b: []

view [
    base 1000x1000 white rate 1 now 
    
    on-create [        
        face/flags: [all-over]

        append draw-block [grid-block: [] grid-block-2: [] crosshair-block: [] translate-block: [translate 0x0] scale-block: [scale 1.0 1.0] candle-block: []] 
        draw-block/grid-block: reduce ['push reduce ['translate 0x0 grid/generate 1000x400 100x100 25x25 Gray 2 AliceBlue 1]]
        ;probe draw-block/grid-block
        
        draw-block/grid-block-2: reduce ['push reduce ['translate 0x500 grid/generate 1000x400 200x200 50x50 Black 2 Cyan 1]]
        ;probe draw-block/grid-block-2

        face/draw: draw-block
    ]

    on-time [
        open: close-old

        ;open: round/to do reduce [open-old pick [+ -] random 2 (open-old * random body-epsilon)] 0.01
        close: round/to do reduce [close-old pick [+ -] random 2 (close-old * random body-epsilon)] 0.01
        
        up?: close >= open
        either up? [
            ;print "UP"
            high: round/to close + (close * random wick-epsilon) 0.01
            low: round/to open - (open * random wick-epsilon) 0.01
        ]
        [
            ;print "DOWN"
            high: round/to open + (open * random wick-epsilon) 0.01
            low: round/to close - (close * random wick-epsilon) 0.01
        ]

        open-old: open
        close-old: close

        open: open * scale        
        high: high * scale
        low: low * scale
        close: close * scale
        ;print ["O:" open "H:" high "L:" low "C:" close]

        timestamp: as-pair 15 * idx 200
        translate: 100

        ;crosshair: ['plot] ;crosshair/generate crosshair-block 1000x200 1000x200

        ;face/draw crosshair
        
        ;print ["crosshair:" crosshair-block]
        ;append face/draw crosshair-block
        ;append face/draw [translate 0x0 scale 1.0 1.0]
        
        if face/draw/candle-block <> none [
            face/draw/candle-block: append face/draw/candle-block candlestick/generate/border/wicks timestamp open + translate high + translate low + translate close + translate volume 10 green red black blue

             open: close-old

        ;open: round/to do reduce [open-old pick [+ -] random 2 (open-old * random body-epsilon)] 0.01
        close: round/to do reduce [close-old pick [+ -] random 2 (close-old * random body-epsilon)] 0.01
        
        up?: close >= open
        either up? [
            ;print "UP"
            high: round/to close + (close * random wick-epsilon) 0.01
            low: round/to open - (open * random wick-epsilon) 0.01
        ]
        [
            ;print "DOWN"
            high: round/to open + (open * random wick-epsilon) 0.01
            low: round/to close - (close * random wick-epsilon) 0.01
        ]

        open-old: open
        close-old: close

        open: open * scale        
        high: high * scale
        low: low * scale
        close: close * scale
        ;print ["O:" open "H:" high "L:" low "C:" close]

        timestamp: as-pair 15 * idx 200
        translate: -500
            face/draw/candle-block: append face/draw/candle-block candlestick/generate/border/wicks timestamp open + translate high + translate low + translate close + translate volume 10 blue yellow red black
        ]

        ;face/draw/crosshair-block: crosshair/generate face/draw/crosshair-block old-offset 1000x100
        
        ;face/draw/candle-block: candlestick/generate/border/wicks timestamp open + translate high + translate low + translate close + translate volume 10 green red black blue
        
        ;append face/draw candlestick/generate/border/wicks timestamp open + translate high + translate low + translate close + translate volume 10 green red black blue
        ;append face/draw bar/generate/border/wicks timestamp open high low close volume 10 green red black blue
        ;translate: -100
        ;append face/draw pnf/generate/border/wicks timestamp open + translate high + translate low + translate close + translate volume 10 green red black blue
        idx: idx + 1     
    ] 
    on-wheel
    [
        s: s + (event/picked * 0.1)
        face/draw/scale-block/2: s
        face/draw/scale-block/3: s
        
    ]
    on-alt-down
    [
        alt-down?: yes
        old-offset: event/offset
        
        face/draw/crosshair-block: crosshair/generate face/draw/crosshair-block old-offset 1000x400
    ]
    on-alt-up
    [
        alt-down?: no

        old-offset: event/offset
        print ["up @ offset:" old-offset]
    ]
    on-down
    [
        left-down?: yes
        old-offset: event/offset
    ]
    on-up
    [   
        left-down?: no

        old-offset: event/offset
        print ["up @ offset:" old-offset]
    ]
    on-over
    [
        case [
            left-down? [
                difference: event/offset - old-offset 
                ;print ["difference:" difference "x" face/draw/2/1 "y" face/draw/2/2]
                face/draw/translate-block/2/1: face/draw/translate-block/2/1 + difference/1
                face/draw/translate-block/2/2: face/draw/translate-block/2/2 + difference/2
                old-offset: event/offset                
            ]
            alt-down? [
                old-offset: event/offset
                face/draw/crosshair-block: crosshair/generate face/draw/crosshair-block old-offset 1000x400
            ]
        ]
    ]
]
