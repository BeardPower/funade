Red [
    Title: "Scale"
    Type:  "Background"
    Needs: 'View
]

scale: context [

    font: make font! [size: 8]
    face: make face! []

    generate: func [       
        position            [pair!]
        size                [pair!]
        value               [integer! float! date!]
        step                [integer! float!] 
        distance            [integer!]
        width               [integer!]
        length              [integer!]
        color               [word! tuple!]        
        font-size           [integer!]
        rotate              [logic!]

        /local
            block           [block!]
            amount          [integer!]
            count           [integer!]
            start           [integer!]
            stop            [integer!]
            label           [string!]
            label-position  [pair!]
            label-size      [pair!]

        return:     [block!]
    ][    
        font/size: font-size
        amount: to-integer size/x / distance
        print ["size: " size/x "distance:" distance "amount:" amount]
        block: copy ['pen color 'font font]

        ;probe type? rotate
        
        if rotate [append block ['rotate -90 position]]

        label: none
        count: 0

        repeat count amount + 1 [
            start: distance * (count - 1)
            label-position: position
            label-position/x: label-position/x + start            
            append block compose ['line-width (width) 'line (label-position) as-pair (label-position/x) (position/y + length)]
            
            label: to-string either (type? value) = float![round/to value 0.01][value]
            label-size: size-text/with face label
            ;probe label-size

            either rotate [
                label-position/x: position/x + length
                label-position/y: label-position/y - start - to-integer (label-size/y * 0.5)
            ]
            [
                label-position/x: label-position/x - to-integer (label-size/x * 0.5)
                label-position/y: label-position/y + length
            ]            
            append block compose ['text (label-position) (label)]

            value: value + step
        ]

        ;probe reduce block
        reduce block        
    ]
]

;view [base 1000x500 draw scale/generate 100x100 500x200 now/date 1 100 1 5 black 10]