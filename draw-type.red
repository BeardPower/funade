Red [
    Title: "Candle stick draw type"
    Needs: 'View
]

;aggreate body [box width height color border-width border-color] wick [line ]
;body [box open close 10 black 1 yellow] wick [line high 10 black] wick [line low 10 yellow]
;aggregate 10min 

;plugin:
draw-type: context [
    rule: [
        set body 'body
        set body-color tuple!
        opt ['wick set wick-color tuple!]
    ]

    generate: function [
        ;timestamp       [date!]
        timestamp       [pair!]
        open            [float!]
        high            [float!]
        low             [float!]
        close           [float!]
        volume          [float!]
        width           [integer!]
        body-color-up   [tuple!]
        body-color-down [tuple!]
        /border
        border-color    [tuple!]
        /wicks
        wick-color      [tuple!]

        /local
            block       [block!]
            left-bottom [pair!]
            right-top   [pair!]
            offset      [integer!]
            up?         [logic!]
            body-color  [tuple!]
            up-wick     [tuple!]
            down-wick   [tuple!]

        return:         [block!]
    ][

        up?: close >= open
        left-bottom: timestamp
        print ["timestamp:" timestamp "offset:" to integer! (open - close)] 
        left-bottom/y: 400 - to integer! either up? [open][close] ;to integer! open; left-bottom/y - to integer! open ;(open - close) ;timestamp ;as-pair to integer! timestamp/second to integer! open
        right-top: left-bottom
        print ["lb:" left-bottom]
        offset: to integer! width * 0.5
        right-top/x: right-top/x + width
        right-top/y: right-top/y - to integer! absolute (close - open)
        print ["rt:" right-top]
        
        either up? [
                body-color: body-color-up
            ]
            [
                body-color: body-color-down
            ]
        block: copy ['pen body-color 'line-width 1 'fill-pen body-color 'box left-bottom right-top]
        print [left-bottom right-top]

        if border [block/2: border-color]
        
        if wicks [
            up-wick: as-pair (right-top/x - offset) (right-top/y - either up? [high - close][high - open])
            down-wick: as-pair (left-bottom/x + offset) (left-bottom/y + either up? [open - low][close - low])
            print ["up-wick:" up-wick "down-wick:" down-wick]
            append block quote ['pen wick-color
                                ;'line-cap 'round
                                'line-width 1
                                'line as-pair (right-top/x - offset) right-top/y up-wick
                                'line as-pair (left-bottom/x + offset) left-bottom/y down-wick
            ]
        ]

        print ["block:" block lf "reduced block:" reduce block]
        reduce block
    ]

    draw-type: function [input [block!]] [
        print ["input:" input]
        probe parse input rule
        print ["body-color:" body-color]
        print ["wick-color:" wick-color]
    ]

    comment {
plugin/draw-type [body 255.255.255.0 wick 135.255.255.0]
plugin/draw-type [body 255.255.255.0]

plugin/generate now 10.0 20.0 0.0 15.0 123.3 10 black
plugin/generate/border now 10.0 20.0 0.0 15.0 123.3 10 black red
plugin/generate/wicks now 10.0 20.0 0.0 15.0 123.3 10 black blue
plugin/generate/border/wicks now 10.0 20.0 0.0 15.0 123.3 10 black red blue
plugin/generate/wicks/border now 10.0 20.0 0.0 15.0 123.3 10 black blue red
}
]
