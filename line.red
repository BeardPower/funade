Red [
    Title: "Line draw type"
    Type: "Drawtype"
    Needs: 'View
]

line: context [
    plot: func [
        data        [block!]

        /local
            rule    [block!]
            block   [block!]
            pattern [block!]

        return:         [block!]
    ][

        rule: [
            set start           [pair!]
            set end             [pair!]
            set width           [integer!]
            set color           [lit-word! | word! | tuple!]
            set style           [lit-word! | word!]
        ]

        block: copy []

        if parse data rule [
            ;pattern: reduce ['pen 'pattern 4x1 [pen yellow line 0x0 2x0]]
            ;block: [pattern 'line-width 1 'line start end]
            block: ['line-width width 'fill-pen 'off 'pen color 'line start end]
        ]

        probe reduce block
        reduce block
    ]
]

;comment {
    view [base 400x400 draw line/plot [100x100 120x230 5 red d]]
;}
