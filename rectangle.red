Red [
    Title: "Box draw type"
    Type: "Drawtype"
    Needs: 'View
]

box: context [
    generate: func [
        left-bottom     [pair!]
        top-right       [pair!]
        color-body      [word! tuple!]
        color-border   [word! tuple!]
        
        /local
            block       [block!]

        return:         [block!]
    ][

        block: compose ['fill-pen (color-body) 'pen (color-border) 'box (left-bottom) (top-right)]   
        reduce block
    ]
]

view [base 400x400 draw box/generate 50x50 100x100 green black]


