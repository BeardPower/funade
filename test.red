Red [
]


draw-block: []
last-id: 1
delimiter: #"_"
begin: #"a"
value: 1
add: func [

][
    name: to-word rejoin [begin delimiter last-id]

    begin: begin + 1
    append draw-block to set-word! name
    append/only draw-block copy []
    append draw-block/(name) value
    value: value + 1
]

add
add
probe draw-block