Red [
    Title: "Dot draw type"
    Type: "Drawtype"
    Needs: 'View
]

dot: context [
    plot: func [
        data                [block!]

        /local
            rule            [block!]
            block           [block!]
            value           [integer! float!]
            position        [pair!]
            width           [integer!]
            border-color    [lit-word! word! tuple!]
            body-color      [lit-word! word! tuple!]

        return:             [block!]
    ][

        rule: [
            set value           [integer! | float!]
            set position        pair!
            set width           integer!
            set border-color    [lit-word! | word! | tuple!]
            set body-color      [lit-word! | word! | tuple!]
        ]

        block: copy []

        if parse data rule [
            block: compose ['fill-pen (body-color) 'pen (border-color) 'circle as-pair position/x position/y - value width * 0.5]
        ]

        reduce block
    ]
]

;comment {
    ;view [base 400x400 draw dot/plot [10 50x50 10 green black]]
    ;view [base 400x400 draw dot/plot [100 100x100 50 yellow red]]
;}
